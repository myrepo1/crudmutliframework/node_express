# Node Express Menu Rest API

This project is part of Multiframework Rest API. I'm using multiple programming languages and frameworks to create same Rest API. For different programming languages, I try to use different approach or stacks.
Expected code should has good documention on how to run the server, how to migrate data and how to test data.
Testing could use postman collection in `test/postman-test-collection.json`. Expected code should pass test via provided postman collection.

This project use Javascript for its programming language with NodeJS and Typescript and postgres for its database.
Some noteable libraries are:
* [DbMate]("https://github.com/amacneil/dbmate" "DbMate Link") for database schema migration
* [Express]("https://expressjs.com/" "ExpressJS Link") for web framework
* [Fast-CSV]("https://c2fo.github.io/fast-csv/", "Fast-CSV Link") for process csv file
* [JSONWebToken]("https://github.com/auth0/node-jsonwebtoken#readme", "JWT Library Link") for handling JWT
* [Zapatos]("https://jawj.github.io/zapatos/" "Zapatos Link") for handling query and database schema
* [Zod]("https://zod.dev/" "Zod Link") for data validation

## How to Run Server
By default server will be running using port 4000, and postgres using port 5432. Please ensure those ports is available. Here are the steps to run the server:
1. copy `.env.example` to `.env`
2. Run `docker-compose up --build`
3. Where there is log `Server is listening on port 4000!`, server is ready to run

## How to Migrate data
By default when running server using docker-compose, database is migrated using script in `entrypoint.sh`, but you can migrate database from local by installing [DbMate]("https://github.com/amacneil/dbmate" "DbMate Link") in local and ensure environment variable `DATABASE_URL` has correct database url.

## How to Test Code
There are two test available:
1. Unit Test and route test. This test using jest library, here are the steps:
    1. Run comment backend service in `docker-compose.yaml` so only db container is running
    2. Run docker-compose up --build
    3. Ensure database is migrated, you can use dbmate from your local
    5. Run `yarn` to install required libraries
    4. Run `yarn test` to execute test
2. Postman collection test
    1. Import `test/postman-test-collection.json` to postman
    2. Ensure database is now so categoryId in csv file are correct
    3. Add file `test/tree-736885__480.jpg` and `valid-menu.csv` to postman default files
    4. Run server using step in How to Run Server
    5. Run collection `Multiframework Test` in Postman