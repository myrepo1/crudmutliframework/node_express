import { Request, Response, Router } from 'express'
import Auth from '../services/auth'

const authRouter = Router()

authRouter.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body
  const result = new Auth().login({ username, password })
  return res.status(result.result ? 200 : 400).json(result)
})

authRouter.post('/refresh', async (req: Request, res: Response) => {
  const { refreshToken } = req.body
  const result = new Auth().refresh(refreshToken)
  return res.status(result.result ? 200 : 400).json(result)
})

export default authRouter
