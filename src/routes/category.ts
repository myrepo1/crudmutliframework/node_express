import { Request, Response, Router } from 'express'
import Category from '../services/category'
import { authMiddleware } from '../middlewares/auth'

const categoryRouter = Router()

categoryRouter.get('/', async (req, res) => {
  const categories = await new Category().list()
  return res.status(categories.result ? 200 : 400).json(categories)
})

categoryRouter.post(
  '/',
  authMiddleware,
  async (
    req: Request<unknown, unknown, { name: string; description?: string }>,
    res: Response,
  ) => {
    const category = await new Category().create(req.body)
    return res.status(category.result ? 201 : 400).json(category)
  },
)

categoryRouter.delete(
  '/',
  authMiddleware,
  async (req: Request, res: Response) => {
    const result = await new Category().empty()
    return res.status(result.result ? 200 : 400).json(result)
  },
)

categoryRouter.put(
  '/category/:id',
  authMiddleware,
  async (
    req: Request<
      { id: string },
      unknown,
      { name: string; description?: string }
    >,
    res: Response,
  ) => {
    const category = await new Category().update({
      ...req.body,
      id: Number(req.params.id),
    })
    return res.status(category.result ? 200 : 400).json(category)
  },
)

categoryRouter.delete(
  '/category/:id',
  authMiddleware,
  async (req: Request<{ id: string }, unknown>, res: Response) => {
    const category = await new Category().delete({ id: Number(req.params.id) })
    return res.status(category.result ? 200 : 400).json(category)
  },
)

export default categoryRouter
