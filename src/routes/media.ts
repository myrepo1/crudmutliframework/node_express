import { Request, Response, Router } from 'express'
import { checkPathExist, getFullPath } from '../helpers/file'

const mediaRouter = Router()

mediaRouter.get('/:fileName', async (req: Request, res: Response) => {
  const filePath = getFullPath(`media/${req.params.fileName}`)
  if (checkPathExist(filePath)) return res.sendFile(filePath)
  return res.status(404).json({ result: false, message: 'File not found' })
})

export default mediaRouter
