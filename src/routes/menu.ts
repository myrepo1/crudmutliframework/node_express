import { Request, Response, Router } from 'express'
import { authMiddleware } from '../middlewares/auth'
import Menu from '../services/menu'
import generateFileMiddleware from '../middlewares/upload'
import { checkPathExist, removeFile } from '../helpers/file'

const menuRouter = Router()

menuRouter.get('/', async (req: Request, res: Response) => {
  const { useCount, downloadData, ...menuParams } = req.query
  if (downloadData === '1') {
    const menu = await new Menu().download(menuParams?.condition as any)
    if (menu.result)
      return res.status(200).sendFile(menu.data, () => {
        removeFile(menu.data)
      })
    return res.status(400).json(menu)
  }
  const menus = await new Menu().list(menuParams, useCount === '1')
  return res.status(menus.result ? 200 : 400).json(menus)
})

menuRouter.post('/', authMiddleware, async (req: Request, res: Response) => {
  const menu = await new Menu().create(req.body)
  return res.status(menu.result ? 201 : 400).json(menu)
})

menuRouter.post(
  '/upload',
  [authMiddleware, generateFileMiddleware('media') as any],
  async (req: Request, res: Response) => {
    const menu = await new Menu().upload(req.file?.path as string)
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

menuRouter.post(
  '/delete',
  authMiddleware,
  async (req: Request, res: Response) => {
    const menu = await new Menu().deleteMany(req.body)
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

menuRouter.get(
  '/menu/:id',
  async (req: Request<{ id: string }>, res: Response) => {
    const menu = await new Menu().detail(Number(req.params.id))
    if (menu.result && !menu.data) return res.status(404).json(menu)
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

menuRouter.put(
  '/menu/:id',
  authMiddleware,
  async (req: Request<{ id: string }, unknown, object>, res: Response) => {
    const menu = await new Menu().update({
      ...req.body,
      id: Number(req.params.id),
    })
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

menuRouter.put(
  '/menu/:id/pic',
  [authMiddleware, generateFileMiddleware('media') as any],
  async (req: Request<{ id: string }, unknown, object>, res: Response) => {
    const menu = await new Menu().update({
      pic: req.file?.path,
      id: Number(req.params.id),
    })
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

menuRouter.delete(
  '/menu/:id',
  authMiddleware,
  async (req: Request<{ id: string }>, res: Response) => {
    const menu = await new Menu().delete(Number(req.params.id))
    return res.status(menu.result ? 200 : 400).json(menu)
  },
)

export default menuRouter
