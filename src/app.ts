import express, { Application } from 'express'
import dotenv from 'dotenv'
import cors from 'cors'
import categoryRouter from './routes/category'
import menuRouter from './routes/menu'
import authRouter from './routes/auth'
import mediaRouter from './routes/media'

import { checkPathExist, createDir } from './helpers/file'

//Prepare media directory
if (!checkPathExist('media')) {
  createDir('media')
}

// Boot express
dotenv.config()
const app: Application = express()
app.use(cors())
app.use(express.json())

// Application routing
app.use('/categories', categoryRouter)
app.use('/menus', menuRouter)
app.use('/auth', authRouter)
app.use('/media', mediaRouter)
app.use('/health', async (req, res) => {
  res.send('OK')
})

export default app
