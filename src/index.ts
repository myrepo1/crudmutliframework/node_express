import app from './app'
import env from './configs/env'

// Boot express
const port = env.port

// Start server
app.listen(port, () => console.log(`Server is listening on port ${port}!`))
