import { camelCase } from 'lodash'

export async function delay(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

function isObject(val: unknown): val is Record<string, unknown> {
  return typeof val === 'object' && val !== null && !Array.isArray(val)
}

/**
 * Converts object keys to camel case.
 * If the input value is an array, it recursively converts the keys of all objects in the array to camel case.
 * If the input value is an object, it recursively converts all the keys of the object to camel case.
 * If the input value is neither an array nor an object, it returns the input value as is.
 * @param value - The input value to convert.
 * @returns The converted value with camel case keys.
 */
export function objectKeyToCamelCase(value: unknown): unknown {
  if (Array.isArray(value)) {
    return value.map((i, j) => {
      return isObject(i) ? objectKeyToCamelCase(i) : i
    })
  } else if (isObject(value)) {
    const result: Record<string, unknown> = {}
    Object.keys(value).forEach((key) => {
      result[camelCase(key)] = isObject(value[key])
        ? objectKeyToCamelCase(value[key])
        : value[key]
    })
    return result
  } else return value
}
