import { writeToPath } from '@fast-csv/format'
import fs from 'fs'
import imgGen from 'js-image-generator'
import { v4 } from 'uuid'

import pool from '../configs/pool'
import { checkPathExist, createDir, getFullPath } from './file'

export async function generalAfterAll() {
  await pool.end()
}

export function generateDummyImage() {
  if (!checkPathExist('media')) {
    createDir('media')
  }
  const filePath = getFullPath(`media/${v4()}.jpg`)
  imgGen.generateImage(800, 600, 80, function (err, image) {
    fs.writeFileSync(filePath, image.data)
  })
  return filePath
}

export async function generateCsvFile(data: Record<string, any>[]) {
  if (!checkPathExist('media')) {
    createDir('media')
  }
  const filePath = getFullPath(`media/${v4()}.csv`)
  return await new Promise((resolve, reject) => {
    writeToPath(filePath, data, { headers: true, delimiter: ';' })
      .on('finish', () => {
        return resolve(filePath)
      })
      .on('error', (e) => {
        return reject(e)
      })
  })
}
