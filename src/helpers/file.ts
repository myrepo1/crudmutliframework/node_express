import { parse } from 'fast-csv'
import fs, { writeFileSync } from 'fs'
import path from 'path'
import { v4 } from 'uuid'
import env from '../configs/env'
import { writeToStream } from '@fast-csv/format'

/**
 * Check current path exist
 * @param filePath -- File path to be checked
 * @returns boolean
 */
export function checkPathExist(filePath: string) {
  const fullPath = getFullPath(filePath)
  return fs.existsSync(fullPath)
}

/**
 * Get absolute path given certain filePath.
 * If path is relative, fill it with current working directory
 * @param filePath
 * @returns string -- Absolute path
 */
export function getFullPath(filePath: string) {
  if (path.isAbsolute(filePath)) return filePath
  return path.join(process.cwd(), filePath)
}

export function createDir(filePath: string) {
  const fullPath = getFullPath(filePath)
  fs.mkdirSync(fullPath, { recursive: true })
}

/**
 * Remove a file or directory.
 * @param dirPath - The path of the file or directory to be removed.
 * @param recursive - If true, remove directory and its contents recursively. Defaults to false.
 * @param force - If true, When true, exceptions will be ignored if path does not exist. Defaults to false.
 */
export function removeFile(dirPath: string, recursive = false, force = false) {
  fs.rmSync(dirPath, { recursive, force })
}

/**
 * Process a CSV file asynchronously. Expected delimiter is ";"
 *
 * @param filePath - The path to the CSV file.
 * @param processFn - The function to process each batch of data.
 */

export async function processCsv(
  filePath: string,
  processFn: (data: unknown[]) => Promise<any>,
) {
  let batchData: unknown[] = []
  let currentRead: Promise<void> | null = null
  // Create a promise to read the data from the CSV file
  const readData = new Promise((resolve, reject) => {
    let rowIndex = 0

    // Create a read stream for the CSV file and parse it with headers and delimiter
    const reader = fs
      .createReadStream(getFullPath(filePath))
      .pipe(parse({ headers: true, delimiter: ';' }))

    // Event handlers for each row of data
    reader
      .on('data', (row: unknown) => {
        batchData.push(row)
        // Check if the batch size limit has been reached
        if ((rowIndex + 1) % env.uploadBatchSize === 0) {
          // Pause reading before execute asynchronous processFn
          reader.pause()
          // Process the batch data asynchronously and resume reading
          currentRead = processFn(batchData)
            .then(() => {
              batchData = []
              reader.resume()
            })
            .catch((e) => {
              return reject(e)
            })
        }
        rowIndex++
      })
      .on('error', (e: Error) => reject(e))
      .on('end', () => resolve(true))
  })
  // Wait for the data to be read
  await readData
  // Wait for the last batch to be processed, if any
  if (currentRead !== null) await currentRead
  else if (batchData.length) {
    if (batchData.length) {
      await processFn(batchData)
    }
  }
}

/**
 * Writes data to a CSV file.
 *
 * @param data - The data to be written to the CSV file.
 * @param filePath - The path of the CSV file. If not provided, a new file will be created in the 'media' directory.
 * @param headers - Whether to include headers in the CSV file. Default is true.
 * @param last - Whether to append a newline character at the end of the CSV file. Default is true.
 * @returns The path of the CSV file.
 */
export async function writeToCsv(
  data: unknown[],
  filePath = '',
  headers = true,
  last = true,
) {
  const pathResult = filePath
    ? getFullPath(filePath)
    : getFullPath(path.join('media', v4() + '.csv'))
  await new Promise((res, rej) => {
    writeToStream(
      fs.createWriteStream(pathResult, { flags: filePath ? 'a' : 'w' }),
      data as any,
      { headers, delimiter: ';' },
    )
      .on('finish', () => {
        return res(true)
      })
      .on('error', (e: Error) => {
        return rej(e)
      })
  })
  if (!last) {
    writeFileSync(pathResult, '\n', { flag: 'a' })
  }
  return pathResult
}

export function createFile(filePath = '') {
  const pathResult = filePath
    ? getFullPath(filePath)
    : path.join('media', v4() + '.csv')
  fs.openSync(pathResult, 'w')
  return pathResult
}
