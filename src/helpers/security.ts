import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import env from '../configs/env'

export const salt = crypto.randomBytes(16).toString('hex')

export function comparePass(pass: string, hashedPass: string) {
  return (
    hashedPass ===
    crypto.pbkdf2Sync(pass, salt, 1000, 64, `sha512`).toString(`hex`)
  )
}

export function hashPass(pass: string) {
  return crypto.pbkdf2Sync(pass, salt, 1000, 64, `sha512`).toString(`hex`)
}

export function generateJWT(
  payload: Record<string, string | number | boolean>,
  expiresIn: string,
  secret: string | null = null,
) {
  return jwt.sign(payload, secret || env.jwtSecret, { expiresIn })
}

export function generateDummyAccessToken(
  type: 'accessToken' | 'refreshToken' = 'accessToken',
  secret: string | null = null,
) {
  return generateJWT({ id: 'admin', type }, '1h', secret)
}

export function validateJWT(token: string) {
  return jwt.verify(token, env.jwtSecret)
}
