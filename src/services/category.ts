import * as db from 'zapatos/db'
import * as s from 'zapatos/schema'
import { z } from 'zod'
import pool from '../configs/pool'
import { ProcessResponse } from './type'

const createCategorySchema = z.object({
  name: z.string().min(1),
  description: z.string().nullish(),
})
const idCategorySchema = z.number().int().positive()
const updateCategorySchema = createCategorySchema.extend({
  name: z.string().min(1).optional(),
  id: idCategorySchema,
})

export default class Category {
  async list(): Promise<ProcessResponse<s.category.JSONSelectable[]>> {
    try {
      const categories = await db.select('category', db.all).run(pool)
      return { result: true, data: categories }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async create(params: {
    name: string
    description?: string
  }): Promise<ProcessResponse<s.category.JSONSelectable>> {
    const parseResult = createCategorySchema.safeParse(params)
    if (!parseResult.success)
      return { result: false, message: parseResult.error.message }
    try {
      const result = await db.insert('category', params).run(pool)
      return { result: true, data: result }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async update(params: {
    name?: string
    description?: string
    id: number
  }): Promise<ProcessResponse<s.category.JSONSelectable>> {
    const parseResult = updateCategorySchema.safeParse(params)
    if (!parseResult.success)
      return { result: false, message: parseResult.error.message }
    const { id, ...payload } = parseResult.data
    try {
      const result = await db.update('category', payload, { id }).run(pool)
      if (!result.length)
        return { result: false, message: 'No category updated' }
      return { result: true, data: result[0] }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async delete({ id }: { id: number }): Promise<ProcessResponse> {
    const parseResult = idCategorySchema.safeParse(id)
    if (!parseResult.success)
      return { result: false, message: parseResult.error.message }
    try {
      const result = await db.deletes('category', { id }).run(pool)
      if (!result.length)
        return { result: false, message: 'No category deleted' }
      return { result: true, data: null }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async empty(): Promise<ProcessResponse> {
    try {
      await db.truncate('category', 'CASCADE').run(pool)
      return { result: true, data: null }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }
}
