import * as db from 'zapatos/db'
import { conditions as dc } from 'zapatos/db'
import * as s from 'zapatos/schema'
import { z } from 'zod'
import pool from '../configs/pool'
import { MenuPicture, ProcessResponse } from './type'
import {
  checkPathExist,
  createFile,
  processCsv,
  removeFile,
  writeToCsv,
} from '../helpers/file'
import path from 'path'
import env from '../configs/env'
import { objectKeyToCamelCase } from '../helpers/general'

const operatorFuncMap = {
  eq: dc.eq,
  ne: dc.ne,
  gt: dc.gt,
  gte: dc.gte,
  lt: dc.lt,
  lte: dc.lte,
  in: dc.isIn,
  like: dc.like,
  ilike: dc.ilike,
} as const

const columns = [
  'id',
  'name',
  'description',
  'price',
  'category_id',
  'status',
  'pic',
] as const
const listColumns = columns.filter((i) => i !== 'description')

const baseMenuSchema = z.object({
  name: z.string().min(1),
  description: z.string().nullish(),
  price: z.number().int().gte(0),
  status: z.boolean().default(false),
  categoryId: z.number().int(),
  pic: z
    .union([
      z.object({ type: z.literal('url'), path: z.string().url() }),
      z.string(),
    ])
    .optional(),
})

const updateMenuSchema = baseMenuSchema
  .partial()
  .extend({ id: z.number().int(), status: z.boolean().optional() })

const conditionItem = z
  .object({
    field: z.enum(columns),
    operator: z.enum([
      'eq',
      'ne',
      'gt',
      'gte',
      'lt',
      'lte',
      'in',
      'like',
      'ilike',
    ]),
    value: z.union([
      z.number(),
      z.string(),
      z.boolean(),
      z.null(),
      z.array(z.string()),
      z.array(z.number()),
    ]),
  })
  .transform((item) => {
    if (item.field === 'status') {
      item.value =
        typeof item.value === 'string' ? item.value === '1' : !!item.value
    } else if (['id', 'price', 'category_id'].includes(item.field)) {
      if (Array.isArray(item.value)) {
        item.value = (item.value as string[]).map((i) => Number(i))
      } else {
        item.value = Number(item.value)
      }
    }
    return item
  })

const conditionSchema = z.object({
  operator: z.enum(['and', 'or']),
  items: z.array(conditionItem).min(1),
})

const sortSchema = z.object({
  field: z.enum(columns),
  direction: z.enum(['ASC', 'DESC']),
})

const querySchema = z.object({
  condition: z.optional(conditionSchema),
  sort: z.array(sortSchema).optional(),
  limit: z.coerce.number().int().optional(),
  offset: z.coerce.number().int().optional(),
})

export default class Menu {
  async list(
    params?: z.infer<typeof querySchema>,
    useCount = false,
  ): Promise<ProcessResponse<unknown[], number>> {
    const parseResult = querySchema.safeParse(params ?? {})
    if (!parseResult.success) {
      return { result: false, message: parseResult.error.message }
    }
    const { condition, sort, limit, offset } = parseResult.data
    const menuCondition = this.parseCondition(condition)

    const options: db.SelectOptionsForTable<
      'menu',
      s.ColumnForTable<'menu'>[],
      db.SQLFragmentMap,
      Record<string, s.ColumnForTable<'menu'>>,
      never
    > = {
      columns: listColumns,
    }
    if (sort?.length) {
      options.order = []
      sort.forEach((item) => {
        (options.order as typeof options.order).push({
          by: item.field,
          direction: item.direction,
        })
      })
    }
    if (limit !== undefined) {
      options.limit = limit
    }
    if (offset !== undefined) {
      options.offset = offset
    }
    try {
      const result = await db.select('menu', menuCondition, options).run(pool)
      let totalMenu = 0
      if (useCount) totalMenu = await db.count('menu', menuCondition).run(pool)
      return {
        result: true,
        data: objectKeyToCamelCase(result) as unknown[],
        count: totalMenu,
      }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }
  async detail(id: number): Promise<ProcessResponse<unknown | undefined>> {
    try {
      const result = await db.selectOne('menu', { id }).run(pool)
      const menuPic = (result?.pic ?? null) as MenuPicture | null
      if (menuPic && menuPic.type == 'file') {
        menuPic.path = this.getMediaUrl(menuPic.path)
      }
      return { result: true, data: objectKeyToCamelCase(result) }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async create(
    payload: z.infer<typeof baseMenuSchema>,
  ): Promise<ProcessResponse<unknown>> {
    const parseResult = baseMenuSchema
      .transform(({ categoryId, ...restData }) => {
        return { category_id: categoryId, ...restData }
      })
      .safeParse(payload)
    if (!parseResult.success) {
      return { result: false, message: parseResult.error.message }
    }
    try {
      const result = await db.insert('menu', parseResult.data).run(pool)
      return { result: true, data: objectKeyToCamelCase(result) }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async update(
    payload: z.infer<typeof updateMenuSchema>,
  ): Promise<ProcessResponse<unknown>> {
    const parseResult = updateMenuSchema
      .transform(({ categoryId, ...restData }) => {
        return categoryId !== undefined
          ? { category_id: categoryId, ...restData }
          : restData
      })
      .safeParse(payload)
    if (!parseResult.success) {
      return { result: false, message: parseResult.error.message }
    }
    try {
      const { id, ...restData } = parseResult.data
      let picData = restData.pic as MenuPicture | string | undefined
      if (picData) {
        if (typeof picData === 'string') {
          if (!checkPathExist(picData))
            return { result: false, message: 'Image not found' }
          picData = { type: 'file', path: picData }
        }
        restData.pic = picData as any
        const result = await db.serializable(pool, async (txn) => {
          const previousData = await db
            .selectExactlyOne('menu', { id })
            .run(txn)
          const result = await db.update('menu', restData, { id }).run(txn)
          if ((previousData.pic as MenuPicture | null)?.type === 'file') {
            removeFile((previousData.pic as MenuPicture).path)
          }
          return result
        })
        if (!result.length) return { result: false, message: 'No menu updated' }
        return { result: true, data: objectKeyToCamelCase(result[0]) }
      } else {
        const result = await db.update('menu', restData, { id }).run(pool)
        if (!result.length) return { result: false, message: 'No menu updated' }
        return { result: true, data: objectKeyToCamelCase(result[0]) }
      }
    } catch (error) {
      if (
        typeof parseResult.data.pic === 'string' ||
        (parseResult.data.pic as MenuPicture)?.type === 'file'
      ) {
        removeFile(
          (parseResult.data.pic as MenuPicture)?.path ||
            (parseResult.data.pic as string),
        )
      }
      return { result: false, message: (error as Error).message }
    }
  }

  async upload(filePath: string): Promise<ProcessResponse> {
    if (!checkPathExist(filePath))
      return { result: false, message: 'File not found' }
    const validator = z
      .array(
        baseMenuSchema
          .extend({
            price: z.coerce.number().int(),
            status: z.coerce.number(),
            categoryId: z.coerce.number().int(),
          })
          .transform(({ categoryId, status, ...restData }) => {
            return {
              category_id: categoryId,
              status: status === 1,
              ...restData,
            }
          }),
      )
      .min(1)
    const processFn = async (data: unknown[]) => {
      const validData = validator.safeParse(data)
      if (!validData.success) {
        throw new Error('Failed parse data')
      }
      await db.insert('menu', validData.data, { returning: [] }).run(pool)
    }
    try {
      await processCsv(filePath, processFn)
      removeFile(filePath)
      return { result: true, data: null }
    } catch (error) {
      removeFile(filePath)
      return { result: false, message: (error as Error).message }
    }
  }

  async download(
    params?: z.infer<typeof conditionSchema>,
  ): Promise<ProcessResponse<string>> {
    const conditionParams = conditionSchema.optional().safeParse(params)
    if (!conditionParams.success)
      return { result: false, message: conditionParams.error.name }
    const menuCondition = this.parseCondition(conditionParams.data)
    try {
      const menuCount = await db.count('menu', menuCondition).run(pool)
      if (menuCount <= env.uploadBatchSize) {
        const menus = await db.select('menu', menuCondition).run(pool)
        const formattedMenu = menus.map((i) => {
          const { pic, category_id, ...rest } = i
          return {
            ...rest,
            pic: pic ? JSON.stringify(pic) : null,
            categoryId: category_id,
          }
        })
        try {
          const fileResult = await writeToCsv(formattedMenu)
          return { result: true, data: fileResult }
        } catch (error) {
          return { result: false, message: (error as Error).message }
        }
      } else {
        const fileResult = createFile()
        try {
          for (let i = 0; i < menuCount; i += env.uploadBatchSize) {
            const currentData = await db
              .select('menu', menuCondition, {
                limit: env.uploadBatchSize,
                offset: i,
              })
              .run(pool)
            if (!currentData.length) continue
            const useHeader = i === 0
            let formattedData: unknown[] | unknown[][] = currentData
            currentData.forEach((menu) => {
              if (menu.pic) {
                menu.pic = JSON.stringify(menu.pic)
              }
              (menu as any).categoryId = menu.category_id
              delete (menu as any).category_id
            })
            if (!useHeader)
              formattedData = formattedData.map((i: any) => Object.values(i))
            await writeToCsv(
              formattedData,
              fileResult,
              useHeader,
              i + env.uploadBatchSize >= menuCount,
            )
          }
        } catch (error) {
          removeFile(fileResult)
          return { result: false, message: (error as Error).message }
        }
        return { result: true, data: fileResult }
      }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async delete(id: number): Promise<ProcessResponse> {
    try {
      const result = await db.deletes('menu', { id }).run(pool)
      if (!result.length) return { result: false, message: 'No menu deleted' }
      if ((result[0].pic as MenuPicture)?.type === 'file') {
        removeFile((result[0].pic as MenuPicture).path)
      }
      return { result: true, data: null }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  async deleteMany(
    condition?: z.infer<typeof conditionSchema>,
  ): Promise<ProcessResponse> {
    try {
      const conditionParams = conditionSchema
        .optional()
        .safeParse(Object.keys(condition || {}).length ? condition : undefined)
      if (!conditionParams.success)
        return { result: false, message: conditionParams.error.message }
      const menuCondition = this.parseCondition(conditionParams.data)
      const result = await db
        .deletes('menu', conditionParams.data ? menuCondition : {}, {
          returning: ['id', 'pic'],
        })
        .run(pool)
      if (!result.length) return { result: false, message: 'No menu deleted' }
      result.forEach((item) => {
        if ((item.pic as MenuPicture)?.type === 'file') {
          removeFile((item.pic as MenuPicture).path)
        }
      })
      return { result: true, data: null }
    } catch (error) {
      return { result: false, message: (error as Error).message }
    }
  }

  private getMediaUrl(mediaPath: string) {
    const fileName = path.basename(mediaPath)
    return `${env.appHost}/media/${fileName}`
  }

  private parseCondition(condition?: z.infer<typeof conditionSchema>) {
    let menuCondition: s.menu.Whereable | db.SQLFragment<boolean | null> =
      db.all
    if (condition) {
      const menuConditionItems: s.menu.Whereable[] = []
      condition.items.forEach((item) => {
        if (item.operator === 'in')
          menuConditionItems.push({
            [item.field]: operatorFuncMap[item.operator](
              item.value as string[],
            ),
          })
        else {
          menuConditionItems.push({
            [item.field]: (operatorFuncMap[item.operator] as any)(item.value),
          })
        }
      })
      menuCondition = dc[condition.operator](...menuConditionItems)
    }
    return menuCondition
  }
}
