import * as db from 'zapatos/db'
import pool from '../configs/pool'
import { generalAfterAll } from '../helpers/jest'
import Category from './category'

afterAll(async () => {
  await generalAfterAll()
})

afterEach(async () => {
  await db.truncate('category', 'CASCADE').run(pool)
})

describe('Category Test Suit', () => {
  describe('Category List Test Suite', () => {
    it('should return list of categories', async () => {
      await db
        .insert('category', { name: 'test', description: 'test' })
        .run(pool)
      const category = new Category()
      const result = await category.list()
      expect(result).toMatchObject({
        result: true,
        data: [{ id: expect.anything(), name: 'test', description: 'test' }],
      })
    })
  })
  describe('Category Create Test Suite', () => {
    it('should return category created', async () => {
      const category = new Category()
      const result = await category.create({
        name: 'test',
        description: 'test',
      })
      expect(result).toMatchObject({
        result: true,
        data: { id: expect.anything(), name: 'test', description: 'test' },
      })
    })
    it('should not create category given empty name', async () => {
      const category = new Category()
      const result = await category.create({ name: '', description: 'test' })
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not create category given category already exists', async () => {
      await db
        .insert('category', { name: 'test', description: 'test' })
        .run(pool)
      const category = new Category()
      const result = await category.create({
        name: 'test',
        description: 'test',
      })
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
  })
  describe('Category Update Test Suite', () => {
    it('should return category updated', async () => {
      const previousCategory = await db
        .insert('category', { name: 'test', description: 'test' })
        .run(pool)
      const category = new Category()
      const result = await category.update({
        name: 'updated',
        description: 'test',
        id: previousCategory.id,
      })
      expect(result).toMatchObject({
        result: true,
        data: { id: expect.anything(), name: 'updated', description: 'test' },
      })
    })
    it('should not update category given empty name', async () => {
      const previousCategory = await db
        .insert('category', { name: 'test', description: 'test' })
        .run(pool)
      const category = new Category()
      const result = await category.update({
        name: '',
        description: 'test',
        id: previousCategory.id,
      })
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not update category given category does not exists', async () => {
      const category = new Category()
      const result = await category.update({
        name: 'updated',
        description: 'test',
        id: 1,
      })
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
  })
  describe('Category Delete Test Suite', () => {
    it('should return category deleted', async () => {
      const previousCategory = await db
        .insert('category', { name: 'test', description: 'test' })
        .run(pool)
      const category = new Category()
      const result = await category.delete({ id: previousCategory.id })
      expect(result).toMatchObject({ result: true, data: null })
    })
    it('should not delete category given category does not exists', async () => {
      const category = new Category()
      const result = await category.delete({ id: 1 })
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
  })
  describe('Category Clear Test Suite', () => {
    it('should delete all category', async () => {
      await db
        .insert('category', [
          { name: 'test', description: 'test' },
          { name: 'test1', description: 'test1' },
        ])
        .run(pool)
      const category = new Category()
      const result = await category.empty()
      expect(result).toMatchObject({ result: true, data: null })
      const categoryCount = await db.count('category', db.all).run(pool)
      expect(categoryCount).toEqual(0)
    })
  })
})
