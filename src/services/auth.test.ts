import { delay } from '../helpers/general'
import { generateJWT, validateJWT } from '../helpers/security'
import Auth from './auth'
import { FailedProcessResponse, SuccessProcessResponse } from './type'

describe('Auth test suite', () => {
  const validPayload = {
    username: 'admin',
    password: 'admin',
  }

  const invalidPayload = {
    username: 'user',
    password: 'password123',
  }
  describe('login test suite', () => {
    it('should return {result:true, data: {accessToken, refreshToken}} when given a valid payload', () => {
      const auth = new Auth()
      const result = auth.login(validPayload) as SuccessProcessResponse<{
        accessToken: string
        refreshToken: string
      }>
      expect(result.result).toBe(true)
      expect(result.data.accessToken).toBeDefined()
      expect(result.data.refreshToken).toBeDefined()
      const decodedAccessToken = validateJWT(result.data.accessToken) as {
        id: string
        type: 'accessToken'
        exp: number
        iat: number
      }
      expect(decodedAccessToken).toEqual({
        id: validPayload.username,
        type: 'accessToken',
        exp: expect.anything(),
        iat: expect.anything(),
      })
      expect(decodedAccessToken.exp - decodedAccessToken.iat).toEqual(60 * 60)
      const decodedRefreshToken = validateJWT(result.data.refreshToken) as {
        id: string
        type: 'refreshToken'
        exp: number
        iat: number
      }
      expect(decodedRefreshToken).toEqual({
        id: validPayload.username,
        type: 'refreshToken',
        exp: expect.anything(),
        iat: expect.anything(),
      })
      expect(decodedRefreshToken.exp - decodedRefreshToken.iat).toEqual(
        60 * 60 * 24 * 7,
      )
    })
    it('should return {result:false, message: "Invalid username or password"} when given an invalid payload', () => {
      const auth = new Auth()
      const result = auth.login(invalidPayload) as FailedProcessResponse
      expect(result.result).toBe(false)
      expect(result.message).toBe('Invalid username or password')
    })
    it('should return {result:false, message: "Invalid username or password"} when given empty username', () => {
      const auth = new Auth()
      const result = auth.login({
        username: '',
        password: '',
      }) as FailedProcessResponse
      expect(result.result).toBe(false)
      expect(result.message).toBe('Invalid username or password')
    })
  })
  describe('refresh token test suite', () => {
    it('should return {result:true, data: {accessToken, refreshToken}} when given a valid refresh token', () => {
      const refreshToken = generateJWT(
        { id: validPayload.username, type: 'refreshToken' },
        '7d',
      )
      const auth = new Auth()
      const result = auth.refresh(refreshToken) as SuccessProcessResponse<{
        accessToken: string
        refreshToken: string
      }>
      expect(result.result).toBe(true)
      expect(result.data.accessToken).toBeDefined()
      expect(result.data.refreshToken).toEqual(refreshToken)
      const decodedAccessToken = validateJWT(result.data.accessToken) as {
        id: string
        type: 'accessToken'
        exp: number
        iat: number
      }
      expect(decodedAccessToken).toEqual({
        id: validPayload.username,
        type: 'accessToken',
        exp: expect.anything(),
        iat: expect.anything(),
      })
      expect(decodedAccessToken.exp - decodedAccessToken.iat).toEqual(60 * 60)
    })
    it('should return {result:false, message: "Invalid refresh token"} when given an invalid refresh token', () => {
      const auth = new Auth()
      const result = auth.refresh('invalidToken') as FailedProcessResponse
      expect(result.result).toBe(false)
      expect(result.message).toBe('Invalid refresh token')
    })
    it('should return {result:false, message: "Invalid refresh token"} when given expired refresh token', async () => {
      const refreshToken = generateJWT(
        { id: validPayload.username, type: 'refreshToken' },
        '500ms',
      )
      await delay(1000)
      const auth = new Auth()
      const result = auth.refresh(refreshToken) as FailedProcessResponse
      expect(result.result).toBe(false)
      expect(result.message).toBe('Invalid refresh token')
    })
  })
})
