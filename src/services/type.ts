export interface SuccessProcessResponse<T = null, U = null> {
  result: true
  data: T
  count?: U
}

export interface FailedProcessResponse<> {
  result: false
  message: string
}

export type ProcessResponse<T = null, U = null> =
  | SuccessProcessResponse<T, U>
  | FailedProcessResponse

export type MenuPicture = { type: 'url' | 'file'; path: string }
