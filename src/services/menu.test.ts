import { faker } from '@faker-js/faker'
import * as db from 'zapatos/db'
import * as s from 'zapatos/schema'
import pool from '../configs/pool'
import {
  generalAfterAll,
  generateCsvFile,
  generateDummyImage,
} from '../helpers/jest'
import Menu from './menu'
import { FailedProcessResponse, SuccessProcessResponse } from './type'
import {
  checkPathExist,
  createDir,
  getFullPath,
  processCsv,
  removeFile,
} from '../helpers/file'
import path from 'path'
import env from '../configs/env'
import { v4 } from 'uuid'

afterAll(async () => {
  await generalAfterAll()
})

afterEach(async () => {
  await db.truncate('menu', 'CASCADE').run(pool)
  await db.truncate('category', 'CASCADE').run(pool)
})

describe('Menu Test Suite', () => {
  describe('Create Menu Test Suite', () => {
    it('should return menu created', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }

      const result = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: true,
        data: {
          id: expect.anything(),
          name: 'test',
          description: 'test',
          price: 10000,
          categoryId: category.id,
          status: true,
        },
      })
      const createdMenu = await db
        .count('menu', { id: result.data.id })
        .run(pool)
      expect(createdMenu).toEqual(1)
    })
    it('should not create menu given empty name', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: '',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const result = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not create menu given category not found', async () => {
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: 1,
        status: true,
      }
      const result = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
  })
  describe('Update Menu Test Suite', () => {
    it('should return menu updated', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const updatePayload = {
        name: 'updated',
        id: initialMenu.data.id,
      }
      const result = (await menu.update(
        updatePayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: true,
        data: {
          id: expect.anything(),
          name: 'updated',
          description: 'test',
          price: 10000,
          categoryId: category.id,
          status: true,
        },
      })
      const updatedMenu = await db
        .count('menu', { id: result.data.id, name: 'updated' })
        .run(pool)
      expect(updatedMenu).toEqual(1)
    })
    it('should update pic with url path given payload with pic url path', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const updatePayload = {
        name: 'updated',
        id: initialMenu.data.id,
        pic: {
          type: 'url' as const,
          path: faker.image.url(),
        },
      }
      const result = await menu.update(updatePayload)
      expect(result.result).toEqual(true)
      const updatedData = await db
        .selectExactlyOne('menu', { id: initialMenu.data.id })
        .run(pool)
      expect(updatedData.pic).toEqual(updatePayload.pic)
    })
    it('should update pic with file path given payload with pic file path', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const fileImage = generateDummyImage()
      expect(checkPathExist(fileImage)).toEqual(true)
      const updatePayload = {
        name: 'updated',
        id: initialMenu.data.id,
        pic: fileImage,
      }
      const result = await menu.update(updatePayload)
      expect(result.result).toEqual(true)
      const updatedData = await db
        .selectExactlyOne('menu', { id: initialMenu.data.id })
        .run(pool)
      removeFile(fileImage)
      expect(updatedData.pic).toEqual({ type: 'file', path: fileImage })
    })
    it('should remove old pic given payload with pic url path', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const initialImage = generateDummyImage()
      expect(checkPathExist(initialImage)).toEqual(true)
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        category_id: category.id,
        status: true,
        pic: {
          type: 'file',
          path: initialImage,
        },
      }
      const initialMenu = await db.insert('menu', createPayload).run(pool)
      const updatePayload = {
        name: 'updated',
        id: initialMenu.id,
        pic: {
          type: 'url' as const,
          path: faker.image.url(),
        },
      }
      const result = await menu.update(updatePayload)
      expect(result.result).toEqual(true)
      const updatedData = await db
        .selectExactlyOne('menu', { id: initialMenu.id })
        .run(pool)
      expect(updatedData.pic).toEqual(updatePayload.pic)
      expect(checkPathExist(initialImage)).toEqual(false)
    })
    it('should not update menu given empty name', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const updatePayload = {
        name: '',
        id: initialMenu.data.id,
      }
      const result = (await menu.update(
        updatePayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not update menu given menu not found', async () => {
      const menu = new Menu()
      const updatePayload = {
        name: 'updated',
        id: 1,
      }
      const result = (await menu.update(
        updatePayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not update menu given category not found', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const updatePayload = {
        name: 'updated',
        id: initialMenu.data.id,
        categoryId: category.id + 1,
      }
      const result = (await menu.update(
        updatePayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
    it('should not update pic with file path given file not found', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const fileImage = getFullPath(`media/${v4()}.jpg`)
      expect(checkPathExist(fileImage)).toEqual(false)
      const updatePayload = {
        name: 'updated',
        id: initialMenu.data.id,
        pic: fileImage,
      }
      const result = await menu.update(updatePayload)
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
      const updatedData = await db
        .selectExactlyOne('menu', { id: initialMenu.data.id })
        .run(pool)
      expect(updatedData.pic).toEqual({})
    })
  })
  describe('Delete Menu Test Suite', () => {
    it('should return menu deleted', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const createPayload = {
        name: 'test',
        description: 'test',
        price: 10000,
        categoryId: category.id,
        status: true,
      }
      const initialMenu = (await menu.create(
        createPayload,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const result = (await menu.delete(
        initialMenu.data.id,
      )) as SuccessProcessResponse
      expect(result).toMatchObject({ result: true, data: null })
      const deletedMenu = await db
        .count('menu', { id: initialMenu.data.id })
        .run(pool)
      expect(deletedMenu).toEqual(0)
    })
    it('should not delete menu given menu not found', async () => {
      const menu = new Menu()
      const result = (await menu.delete(1)) as SuccessProcessResponse
      expect(result).toMatchObject({
        result: false,
        message: expect.anything(),
      })
    })
  })
  describe('List Menu Test Suite', () => {
    it('should return all menu given list with no parameter', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: s.menu.Insertable[] = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.list()) as SuccessProcessResponse<
        s.menu.JSONSelectable[]
      >
      const expectedMenu = menuPayload.map((i) => {
        const { description, category_id, ...restData } = i
        return { ...restData, categoryId: category_id }
      })
      expect(result).toMatchObject({
        result: true,
        data: expectedMenu,
        count: 0,
      })
    })
    it('should return filtered menu given list with certain condition and menus count given useQuery', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: (s.menu.Insertable & { price: number })[] = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 15000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 12000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu4',
          description: 'test',
          price: 12000,
          category_id: category.id,
          status: false,
        },
      ]
      await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.list(
        {
          condition: {
            operator: 'and',
            items: [{ field: 'status', operator: 'eq', value: '1' }],
          },
          sort: [{ field: 'price', direction: 'DESC' }],
        },
        true,
      )) as SuccessProcessResponse<s.menu.JSONSelectable[]>
      const expectedMenu = menuPayload
        .filter((menu) => menu.status)
        .sort((a, b) => b.price - a.price)
        .map((i) => {
          const { description, category_id, ...restData } = i
          return { ...restData, categoryId: category_id }
        })
      expect(result).toMatchObject({
        result: true,
        data: expectedMenu,
        count: 3,
      })
    })
    it('should return empty menu given condition not met', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: s.menu.Insertable[] = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.list({
        condition: {
          operator: 'and',
          items: [{ field: 'status', operator: 'eq', value: false }],
        },
      })) as SuccessProcessResponse<s.menu.JSONSelectable[]>
      expect(result).toEqual({ result: true, data: [], count: 0 })
    })
    it('should return filtered menu given list with limit and offset', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: (s.menu.Insertable & { price: number })[] = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 15000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 12000,
          category_id: category.id,
        },
      ]
      await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.list({
        limit: 2,
        offset: 1,
        sort: [{ field: 'price', direction: 'ASC' }],
      })) as SuccessProcessResponse<s.menu.JSONSelectable[]>
      const expectedMenu = menuPayload
        .sort((a, b) => a.price - b.price)
        .slice(1, 3)
        .map((i) => {
          const { description, category_id, ...restData } = i
          return { ...restData, categoryId: category_id }
        })

      expect(result).toMatchObject({ result: true, data: expectedMenu })
    })
    it('should failed return menu given wrong condition', async () => {
      const menu = new Menu()
      const result = (await menu.list({
        condition: {
          operator: 'and',
          items: [{ field: 'status', operator: 'eq' } as any],
        },
      })) as FailedProcessResponse
      expect(result).toEqual({ result: false, message: expect.anything() })
    })
    it('should failed return menu given in operator use not array value', async () => {
      const menu = new Menu()
      const result = (await menu.list({
        condition: {
          operator: 'and',
          items: [{ field: 'price', operator: 'like', value: null }],
        },
      })) as FailedProcessResponse
      expect(result).toEqual({ result: false, message: expect.anything() })
    })
  })
  describe('Find Menu Test Suite', () => {
    it('should return menu given id', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: s.menu.Insertable = {
        name: 'menu1',
        description: 'test',
        price: 10000,
        category_id: category.id,
        status: true,
      }
      const newMenu = await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.detail(
        newMenu.id,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      const { category_id, ...expectedPayload } = menuPayload
      expect(result).toMatchObject({
        result: true,
        data: { id: newMenu.id, ...expectedPayload, categoryId: category_id },
      })
    })
    it('should return menu with pic url given menu has pic with type file', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const image = generateDummyImage()
      const menuPayload: s.menu.Insertable = {
        name: 'menu1',
        description: 'test',
        price: 10000,
        category_id: category.id,
        status: true,
        pic: { type: 'file', path: image },
      }
      const newMenu = await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.detail(
        newMenu.id,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      menuPayload.pic = { type: 'file', path: getMediaUrl(image) }
      removeFile(image)
      const { category_id, ...expectedPayload } = menuPayload
      expect(result).toMatchObject({
        result: true,
        data: { id: newMenu.id, ...expectedPayload, categoryId: category_id },
      })
    })
    it('should return undefined menu given menu not found', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menu = new Menu()
      const menuPayload: s.menu.Insertable = {
        name: 'menu1',
        description: 'test',
        price: 10000,
        category_id: category.id,
        status: true,
      }
      const newMenu = await db.insert('menu', menuPayload).run(pool)
      const result = (await menu.detail(
        newMenu.id + 1,
      )) as SuccessProcessResponse<s.menu.JSONSelectable>
      expect(result).toEqual({ result: true, data: undefined })
    })
  })
  describe('Upload Menu Test Suite', () => {
    it('should upload menu given csv file', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          categoryId: category.id,
          status: 1,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          categoryId: category.id,
          status: 1,
        },
      ]
      const filePath = (await generateCsvFile(menuData)) as string
      const result = await new Menu().upload(filePath)
      expect(result).toEqual({ result: true, data: null })
      const insertedMenu = await db.select('menu', db.all).run(pool)
      expect(insertedMenu.length).toEqual(menuData.length)
      const expectedMenu = menuData.map((i) => {
        const { categoryId, status, ...data } = i
        return { ...data, category_id: categoryId, status: status === 1 }
      })
      expect(insertedMenu).toMatchObject(expectedMenu)
      expect(checkPathExist(filePath)).toEqual(false)
    })
    it('should upload menu given csv file with data more than upload batch size', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)

      const menuData = []
      for (let i = 0; i < env.uploadBatchSize * 2; i++) {
        menuData.push({
          name: faker.word.noun(),
          description: faker.lorem.lines(1),
          price: faker.number.int({ min: 10000, max: 5000000 }),
          categoryId: category.id,
          status: 1,
        })
      }
      const filePath = (await generateCsvFile(menuData)) as string
      const result = await new Menu().upload(filePath)
      expect(result).toEqual({ result: true, data: null })
      const insertedMenu = await db.count('menu', db.all).run(pool)
      expect(insertedMenu).toEqual(env.uploadBatchSize * 2)
      expect(checkPathExist(filePath)).toEqual(false)
    })
    it('should not upload menu given incorrect payload', async () => {
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          status: 1,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          status: 1,
        },
      ]
      const filePath = (await generateCsvFile(menuData)) as string
      const result = await new Menu().upload(filePath)
      expect(result).toEqual({ result: false, message: expect.anything() })
      const insertedMenu = await db.select('menu', db.all).run(pool)
      expect(insertedMenu.length).toEqual(0)
      expect(checkPathExist(filePath)).toEqual(false)
    })
    it('should not upload menu given category not found', async () => {
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          status: 1,
          categoryId: 2,
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          status: 1,
          categoryId: 2,
        },
      ]
      const filePath = (await generateCsvFile(menuData)) as string
      const result = await new Menu().upload(filePath)
      expect(result).toEqual({ result: false, message: expect.anything() })
      const insertedMenu = await db.select('menu', db.all).run(pool)
      expect(insertedMenu.length).toEqual(0)
      expect(checkPathExist(filePath)).toEqual(false)
    })
    it('should not upload given file not found', async () => {
      const filePath = getFullPath(`media/${v4()}.csv`)
      const result = await new Menu().upload(filePath)
      expect(result).toEqual({ result: false, message: expect.anything() })
      expect(checkPathExist(filePath)).toEqual(false)
    })
  })
  describe('Download Menu Test Suite', () => {
    if (!checkPathExist('media')) {
      createDir('media')
    }
    it('should download menu given csv file', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().download({
        operator: 'and',
        items: [{ field: 'price', operator: 'lt', value: 15000 }],
      })) as SuccessProcessResponse<string>
      expect(result).toEqual({ result: true, data: expect.anything() })
      expect(checkPathExist(result.data)).toEqual(true)
      const downloadResult: unknown[] = []
      const getData = async (data: unknown[]) => downloadResult.push(...data)
      await processCsv(result.data, getData)
      removeFile(result.data)
      const expectedMenu = menuData.map((i) => {
        const { status, category_id, pic, price, ...data } = i
        return {
          ...data,
          status: status ? 'true' : 'false',
          pic: pic ? JSON.stringify(pic) : JSON.stringify({}),
          price: price.toString(),
          categoryId: category_id.toString(),
        }
      })
      expect(downloadResult).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.anything(),
            ...expectedMenu[0],
          }),
          expect.objectContaining({
            id: expect.anything(),
            ...expectedMenu[1],
          }),
        ]),
      )
    })
    it('should download menu more the upload batch size', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = []
      for (let i = 0; i < env.uploadBatchSize * 2; i++) {
        menuData.push({
          name: faker.word.noun(),
          description: faker.lorem.lines(1),
          price: faker.number.int({ min: 10000, max: 5000000 }),
          category_id: category.id,
          status: true,
        })
      }
      await db.insert('menu', menuData).run(pool)
      const result =
        (await new Menu().download()) as SuccessProcessResponse<string>
      expect(result).toEqual({ result: true, data: expect.anything() })
      expect(checkPathExist(result.data)).toEqual(true)
      const downloadResult: unknown[] = []
      const getData = async (data: unknown[]) => downloadResult.push(...data)
      await processCsv(result.data, getData)
      removeFile(result.data)
      expect(downloadResult.length).toEqual(menuData.length)
    })
    it('should return empty csv file given empty result', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().download({
        operator: 'and',
        items: [{ field: 'price', operator: 'gt', value: 30000 }],
      })) as SuccessProcessResponse<string>
      expect(result).toEqual({ result: true, data: expect.anything() })
      expect(checkPathExist(result.data)).toEqual(true)
      const downloadResult: unknown[] = []
      const getData = async (data: unknown[]) => downloadResult.push(...data)
      await processCsv(result.data, getData)
      removeFile(result.data)
      expect(downloadResult).toHaveLength(0)
    })
    it('should not download menu given invalid condition', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().download({
        operator: 'and',
        items: [{ field: 'status', operator: 'like', value: category.id + 1 }],
      })) as FailedProcessResponse
      expect(result).toEqual({ result: false, message: expect.anything() })
    })
  })
  describe('Delete Many Menu Test Suite', () => {
    if (!checkPathExist('media')) {
      createDir('media')
    }
    it('should delete all menus given empty condition', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: false,
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().deleteMany()) as SuccessProcessResponse
      expect(result).toEqual({ result: true, data: null })
      const menuCount = await db.count('menu', db.all).run(pool)
      expect(menuCount).toEqual(0)
    })
    it('should delete some menus include its pic file if exist given certain condition', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const picFile = generateDummyImage()
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: picFile,
            type: 'file',
          },
        },
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: false,
        },
      ]
      expect(checkPathExist(picFile)).toEqual(true)
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().deleteMany({
        operator: 'and',
        items: [{ field: 'status', operator: 'eq', value: true }],
      })) as SuccessProcessResponse
      expect(result).toEqual({ result: true, data: null })
      const updatedMenus = await db.select('menu', db.all).run(pool)
      expect(updatedMenus).toEqual([
        {
          name: 'menu3',
          description: 'test',
          price: 20000,
          category_id: category.id,
          status: false,
          pic: {},
          id: expect.anything(),
        },
      ])
      expect(checkPathExist(picFile)).toEqual(false)
    })
    it('should fail delete menus given no menu deleted', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
        {
          name: 'menu2',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().deleteMany({
        operator: 'and',
        items: [{ field: 'status', operator: 'eq', value: false }],
      })) as FailedProcessResponse
      expect(result).toEqual({ result: false, message: expect.anything() })
      const menuCount = await db.count('menu', db.all).run(pool)
      expect(menuCount).toEqual(menuData.length)
    })
    it('should not delete menus invalid condition', async () => {
      const category = await db
        .insert('category', { name: 'new-cat', description: 'test' })
        .run(pool)
      const menuData = [
        {
          name: 'menu1',
          description: 'test',
          price: 10000,
          category_id: category.id,
          status: true,
          pic: {
            path: 'url',
            type: faker.image.url(),
          },
        },
      ]
      await db.insert('menu', menuData).run(pool)
      const result = (await new Menu().deleteMany({
        operator: 'and',
        items: [{ field: 'status', operator: 'like', value: category.id + 1 }],
      })) as FailedProcessResponse
      expect(result).toEqual({ result: false, message: expect.anything() })
      const menuCount = await db.count('menu', db.all).run(pool)
      expect(menuCount).toEqual(menuData.length)
    })
  })
})

function getMediaUrl(mediaPath: string) {
  const fileName = path.basename(mediaPath)
  return `${env.appHost}/media/${fileName}`
}
