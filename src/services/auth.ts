import * as z from 'zod'
import { ProcessResponse } from './type'
import {
  comparePass,
  generateJWT,
  hashPass,
  validateJWT,
} from '../helpers/security'
import { JwtPayload } from 'jsonwebtoken'

/**
 * Dummy user has username "admin" and password "admin"
 */

const loginSchema = z.object({
  username: z.string().min(1),
  password: z.string().min(1),
})

const hashedPassword = hashPass('admin')

export default class Auth {
  login(
    payload: z.infer<typeof loginSchema>,
  ): ProcessResponse<{ accessToken: string; refreshToken: string }> {
    const parseResult = loginSchema.safeParse(payload)
    if (!parseResult.success)
      return { result: false, message: 'Invalid username or password' }
    if (
      parseResult.data.username !== 'admin' ||
      !comparePass(parseResult.data.password, hashedPassword)
    )
      return { result: false, message: 'Invalid username or password' }
    const token = generateJWT(
      { id: parseResult.data.username, type: 'accessToken' },
      '1h',
    )
    const refreshToken = generateJWT(
      { id: parseResult.data.username, type: 'refreshToken' },
      '7d',
    )
    return {
      result: true,
      data: { accessToken: token, refreshToken: refreshToken },
    }
  }

  refresh(
    refreshToken: string,
  ): ProcessResponse<{ accessToken: string; refreshToken: string }> {
    if (!z.string().min(1).safeParse(refreshToken).success)
      return { result: false, message: 'Invalid refresh token' }
    try {
      const decodedToken = validateJWT(refreshToken) as JwtPayload
      if ((decodedToken as JwtPayload).type !== 'refreshToken')
        return { result: false, message: 'Invalid refresh token' }
      const token = generateJWT(
        { id: decodedToken.id, type: 'accessToken' },
        '1h',
      )
      return {
        result: true,
        data: { accessToken: token, refreshToken: refreshToken },
      }
    } catch (error) {
      return { result: false, message: 'Invalid refresh token' }
    }
  }
}
