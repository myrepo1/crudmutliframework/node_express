import { Pool } from 'pg'
import env from './env'

const pool = new Pool({
  host: env.pgHost,
  user: env.pgUser,
  password: env.pgPass,
  database: env.pgDb,
  max: 20,
})

export default pool
