import { Request, Response, NextFunction } from 'express'
import { validateJWT } from '../helpers/security'
import { JwtPayload } from 'jsonwebtoken'

export function authMiddleware(
  req: Request & { user?: JwtPayload },
  res: Response,
  next: NextFunction,
) {
  const accessToken = req.headers['authorization']

  if (!accessToken) {
    return res
      .status(401)
      .json({ result: false, message: 'Missing access token' })
  }

  try {
    const decodedToken = validateJWT(
      accessToken.replace('Bearer ', ''),
    ) as JwtPayload
    if (decodedToken.type !== 'accessToken') {
      return res
        .status(401)
        .json({ result: false, message: 'Invalid access token' })
    }
    req.user = decodedToken
    next()
  } catch (error) {
    return res
      .status(401)
      .json({ result: false, message: 'Invalid access token' })
  }
}
