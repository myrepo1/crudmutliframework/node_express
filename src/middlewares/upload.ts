require('dotenv/config')
import multer from 'multer'
import path from 'path'
import { v4 } from 'uuid'
import { checkPathExist, createDir, getFullPath } from '../helpers/file'

const generateDiskStorage = (container: string) => {
  const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
      if (!checkPathExist(container)) {
        createDir(container)
      }
      cb(null, getFullPath(container))
    },
    filename: function (req, file, cb) {
      const filename = v4()
      cb(null, filename + path.extname(file.originalname))
    },
  })
  return diskStorage
}

const generateFileMiddleware = (container: string) => {
  const diskStorage = generateDiskStorage(container)
  return multer({ storage: diskStorage }).single(container)
}

export default generateFileMiddleware
