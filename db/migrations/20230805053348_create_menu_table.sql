-- migrate:up
CREATE TABLE menu (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT,
    price INTEGER NOT NULL DEFAULT 0,
    pic jsonb DEFAULT '{}',
    status BOOLEAN NOT NULL DEFAULT FALSE,
    category_id INTEGER REFERENCES category(id) ON UPDATE CASCADE ON DELETE SET NULL
);

-- migrate:down

