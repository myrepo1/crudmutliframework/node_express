import path from 'path';
import request from 'supertest';
import { v4 } from 'uuid';

import app from "../../../src/app"
import {generateDummyImage} from "../../../src/helpers/jest"
import { removeFile } from '../../../src/helpers/file';

let imageFile = '';

beforeEach(()=>{
    imageFile = generateDummyImage()
})

afterEach(()=>{
    removeFile(imageFile)
})

describe('Test Get Menu Pic',()=>{
    it('should get menu from /media/:fileName', async () => {
        const res = await request(app).get(`/media/${path.basename(imageFile)}`)
        expect(res.statusCode).toEqual(200)
        expect(res.headers['content-type']).toEqual('image/jpeg')       
    })
    it('should failed get menu file not found', async () => {
        const res = await request(app).get(`/media/${v4()}.jpg`)
        expect(res.statusCode).toEqual(404)
    })
})