const mockMenu = jest.fn();
const mockMenuDeleteMany = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {deleteMany:mockMenuDeleteMany}});
const expectedResult = {result:true,data:null}
mockMenuDeleteMany.mockImplementation(async ()=>{return expectedResult});

describe('Test Delete many menus',()=>{
    it('should delete menus from /menus/delete', async () => {
        const payload = {items:[{field:"name",operator:"eq",value:"sample"}],operator:"and"}
        const token = generateDummyAccessToken();
        const res = await request(app).post('/menus/delete').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuDeleteMany).toBeCalledTimes(1)
        expect(mockMenuDeleteMany.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 400 given failed delete many menus', async ()=>{
        const expectedFailedList = {result:false,message:'failed'}
        mockMenuDeleteMany.mockImplementationOnce(async ()=>{
            return expectedFailedList
        })
        const payload = {items:[{field:"name",operator:"eq",value:"sample"}],operator:"and"}
        const token = generateDummyAccessToken();
        const res = await request(app).post('/menus/delete').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedList)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuDeleteMany).toBeCalledTimes(1)
        expect(mockMenuDeleteMany.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 401 given no token provided', async ()=>{
        const res = await request(app).post('/menus/delete')
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockMenuDeleteMany).toBeCalledTimes(0)
    })
})