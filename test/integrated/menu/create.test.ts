const mockMenu = jest.fn();
const mockCreateMenu = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {create:mockCreateMenu}});
const expectedResult = {result:true,data:{id:1,name:'test',description:'test',price:10000,categoryId:1}}
mockCreateMenu.mockImplementation(async ()=>{return expectedResult});

describe('Test Create Menu',()=>{
    it('should create menu from post /menus', async () => {
        const payload = {name:'test',description:'test',price:10000,categoryId:1}
        const token = generateDummyAccessToken()
        const res = await request(app).post('/menus').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(201)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockCreateMenu).toBeCalledTimes(1)
        expect(mockCreateMenu.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 400 given failed create menu', async ()=>{
        const payload = {name:'test',description:'test',price:10000,categoryId:1}
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockCreateMenu.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).post('/menus').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockCreateMenu).toBeCalledTimes(1)
        expect(mockCreateMenu.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const payload = {name:'test',description:'test',price:10000,categoryId:1}
        const res = await request(app).post('/menus').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockCreateMenu).toBeCalledTimes(0)
    })
})