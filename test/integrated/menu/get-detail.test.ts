const mockMenu = jest.fn();
const mockMenuDetail = jest.fn();

import {faker} from '@faker-js/faker';
import request from 'supertest';
import qs from 'qs';
import app from "../../../src/app"

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {detail:mockMenuDetail}});
const expectedResult = {result:true,data:{id:1,name:'test',description:'test',price:10000,categoryId:1}}
mockMenuDetail.mockImplementation(async ()=>{return expectedResult});

describe('Test Find Menu',()=>{
    it('should get menu detail from menus/menu/:id', async () => {
        const menuId = faker.number.int();
        const res = await request(app).get(`/menus/menu/${menuId}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuDetail).toBeCalledTimes(1)
        expect(mockMenuDetail.mock.calls[0][0]).toEqual(menuId)
    })
    it('should get response with status 404 given menu not found', async ()=>{
        const expectedEmptyResult = {result:true,data:undefined}
        mockMenuDetail.mockImplementationOnce(async ()=>{
            return expectedEmptyResult
        })
        const menuId = faker.number.int();
        const res = await request(app).get(`/menus/menu/${menuId}`)
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual(expectedEmptyResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuDetail).toBeCalledTimes(1)
        expect(mockMenuDetail.mock.calls[0][0]).toEqual(menuId)
    })
    it('should get response with status 400 given failed get menu', async ()=>{
        const expectedFailedResult = {result:false,message:'failed'}
        mockMenuDetail.mockImplementationOnce(async ()=>{
            return expectedFailedResult
        })
        const menuId = faker.number.int();
        const res = await request(app).get(`/menus/menu/${menuId}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuDetail).toBeCalledTimes(1)
        expect(mockMenuDetail.mock.calls[0][0]).toEqual(menuId)
    })
})