const mockMenu = jest.fn();
const mockMenuList = jest.fn();
const mockMenuDownload = jest.fn();

import fs from 'fs';
import request from 'supertest';
import qs from 'qs';
import app from "../../../src/app"
import { checkPathExist, getFullPath, processCsv, removeFile, writeToCsv } from '../../../src/helpers/file';
import path from 'path';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {list:mockMenuList,download:mockMenuDownload}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test',price:10000,categoryId:1}]}
mockMenuList.mockImplementation(async ()=>{return expectedResult});

describe('Test List Menu',()=>{
    it('should get menus from /menus', async () => {
        const queryParams = {
            sort:[{field:"price",direction:"ASC"}],
            limit:"10",
            offset:"0",
            condition:{items:[{field:"name",operator:"eq",value:"sample"}],operator:"and"},
            useCount:"1"
        }
        const {useCount,...expectedParams} = queryParams

        const res = await request(app).get('/menus').query(qs.stringify(queryParams))
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuList).toBeCalledTimes(1)
        expect(mockMenuList.mock.calls[0][0]).toEqual(expectedParams)
        expect(mockMenuList.mock.calls[0][1]).toEqual(true)
    })
    it('should get response with status 400 given failed get menu params', async ()=>{
        const expectedFailedList = {result:false,message:'failed'}
        mockMenuList.mockImplementationOnce(async ()=>{
            return expectedFailedList
        })
        const res = await request(app).get('/menus')
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedList)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockMenuList).toBeCalledTimes(1)
        expect(mockMenuList.mock.calls[0][0]).toEqual({})
        expect(mockMenuList.mock.calls[0][1]).toEqual(false)
    })
    it('should download menu given downloadData=1', async ()=>{
        const sourceData = [{id:1,name:'test',description:'test',price:10000,category_id:1},{id:2,name:'test',description:'test',price:10000,category_id:1}]
        const sourceFilePath = await writeToCsv(sourceData)
        const expectedData = sourceData.map(i=>{
            const {id,price,category_id,...rest} = i;
            return {id:id.toString(),price:price.toString(),category_id:category_id.toString(),...rest}
        })
        mockMenuDownload.mockImplementationOnce(async ()=>{
            return {result:true,data:sourceFilePath}
        })
        const queryParams = {
            condition:{items:[{field:"name",operator:"eq",value:"sample"}],operator:"and"},
            downloadData:"1"
        }
        const res = await request(app).get('/menus').query(qs.stringify(queryParams))
        expect(res.statusCode).toEqual(200)
        expect(res.header["content-type"]).toContain('text/csv');
        expect(checkPathExist(sourceFilePath)).toEqual(false);
        const resultFilePath = getFullPath(path.join('media','downloaded-menu.csv'))
        fs.writeFileSync(resultFilePath,res.text)
        const dataResult:any[] = [];
        const processData = async (i:any) => dataResult.push(...i);
        await processCsv(resultFilePath,processData);
        removeFile(resultFilePath)
        expect(mockMenu).toHaveBeenCalledTimes(1)
        expect(mockMenuDownload).toHaveBeenCalledTimes(1)
        expect(mockMenuDownload.mock.calls[0][0]).toEqual(queryParams.condition)
        expect(dataResult).toEqual(expectedData)
    })
    it('should give response with status 400 given failed download menu', async ()=>{
        const expectedFailedResponse = {result:false,message:'failed'}
        mockMenuDownload.mockImplementationOnce(async ()=>{
            return expectedFailedResponse
        })
        const queryParams = {
            condition:{items:[{field:"name",operator:"eq",value:"sample"}],operator:"and"},
            downloadData:"1"
        }
        const res = await request(app).get('/menus').query(qs.stringify(queryParams))
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedResponse)
        expect(mockMenu).toHaveBeenCalledTimes(1)
        expect(mockMenuDownload).toHaveBeenCalledTimes(1)
        expect(mockMenuDownload.mock.calls[0][0]).toEqual(queryParams.condition)
    })
})