const mockMenu = jest.fn();
const mockUpdateMenu = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';
import {generateDummyImage} from "../../../src/helpers/jest"
import { checkPathExist, removeFile } from '../../../src/helpers/file';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {update:mockUpdateMenu}});
const expectedResult = {result:true,data:{id:1,name:'updated',description:'test',price:10000,categoryId:1}}
mockUpdateMenu.mockImplementation(async ()=>{return expectedResult});

let imageFile = '';

beforeEach(()=>{
    imageFile = generateDummyImage()
})

afterEach(()=>{
    removeFile(imageFile)
})

describe('Test Update Menu Pic',()=>{
    it('should update menu from put menus/menu/:id/pic', async () => {
        const menuId = 2;
        const token = generateDummyAccessToken()
        const res = await request(app).put(`/menus/menu/${menuId}/pic`).set('authorization', `Bearer ${token}`).field('Content-Type', 'multipart/form-data').attach('media',imageFile)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)        
        const savedImage = mockUpdateMenu.mock.calls[0][0].pic
        expect(checkPathExist(savedImage)).toEqual(true)
        removeFile(savedImage)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu.mock.calls[0][0]).toEqual({id:menuId,pic:expect.anything()})
    })
    it('should get response with status 400 given failed update menu', async ()=>{
        const menuId = 2;
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockUpdateMenu.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).put(`/menus/menu/${menuId}/pic`).set('authorization', `Bearer ${token}`).field('Content-Type', 'multipart/form-data').attach('media',imageFile)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        const savedImage = mockUpdateMenu.mock.calls[0][0].pic
        expect(checkPathExist(savedImage)).toEqual(true)
        removeFile(savedImage)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu.mock.calls[0][0]).toEqual({id:menuId,pic:expect.anything()})
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const menuId = 2;
        const res = await request(app).put(`/menus/menu/${menuId}/pic`).field('Content-Type', 'multipart/form-data').attach('media',imageFile)
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockUpdateMenu).toBeCalledTimes(0)
    })
})