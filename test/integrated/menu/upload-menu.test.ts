const mockMenu = jest.fn();
const mockUploadMenu = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';
import {generateCsvFile} from "../../../src/helpers/jest"
import { checkPathExist, removeFile } from '../../../src/helpers/file';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {upload:mockUploadMenu}});
const expectedResult = {result:true,data:null}
mockUploadMenu.mockImplementation(async ()=>{return expectedResult});

let csvFile = '';

beforeEach(async ()=>{
    const sampleData = [{id:1,name:'test',description:'test',price:10000,categoryId:1}]
    csvFile = await generateCsvFile(sampleData) as string
})

afterEach(()=>{
    removeFile(csvFile)
})

describe('Test Upload Menu',()=>{
    it('should upload menu from post /menus/upload', async () => {
        const token = generateDummyAccessToken()
        const res = await request(app).post(`/menus/upload`).set('authorization', `Bearer ${token}`).field('Content-Type', 'multipart/form-data').attach('media',csvFile)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)        
        const savedCsv = mockUploadMenu.mock.calls[0][0]
        expect(checkPathExist(savedCsv)).toEqual(true)
        removeFile(savedCsv)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUploadMenu).toBeCalledTimes(1)
        expect(mockUploadMenu.mock.calls[0][0]).toEqual(savedCsv)
    })
    it('should get response with status 400 given failed upload menu', async ()=>{
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockUploadMenu.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).post(`/menus/upload`).set('authorization', `Bearer ${token}`).field('Content-Type', 'multipart/form-data').attach('media',csvFile)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        const savedCsv = mockUploadMenu.mock.calls[0][0]
        expect(checkPathExist(savedCsv)).toEqual(true)
        removeFile(savedCsv)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUploadMenu).toBeCalledTimes(1)
        expect(mockUploadMenu.mock.calls[0][0]).toEqual(savedCsv)
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const res = await request(app).post(`/menus/upload`).field('Content-Type', 'multipart/form-data').attach('media',csvFile)
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockUploadMenu).toBeCalledTimes(0)
    })
})