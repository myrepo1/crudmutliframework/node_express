const mockMenu = jest.fn();
const mockDeleteMenu = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {delete:mockDeleteMenu}});
const expectedResult = {result:true,data:null}
mockDeleteMenu.mockImplementation(async ()=>{return expectedResult});

describe('Test Delete Menu',()=>{
    it('should delete menu from put menus/menu/:id', async () => {
        const menuId = 2;
        const token = generateDummyAccessToken()
        const res = await request(app).delete(`/menus/menu/${menuId}`).set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockDeleteMenu).toBeCalledTimes(1)
        expect(mockDeleteMenu.mock.calls[0][0]).toEqual(menuId)
    })
    it('should get response with status 400 given failed delete menu', async ()=>{
        const menuId = 2;
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockDeleteMenu.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).delete(`/menus/menu/${menuId}`).set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockDeleteMenu).toBeCalledTimes(1)
        expect(mockDeleteMenu.mock.calls[0][0]).toEqual(menuId)
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const menuId = 2;
        const res = await request(app).delete(`/menus/menu/${menuId}`)
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockDeleteMenu).toBeCalledTimes(0)
    })
})