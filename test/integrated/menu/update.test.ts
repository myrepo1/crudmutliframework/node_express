const mockMenu = jest.fn();
const mockUpdateMenu = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/menu',()=>mockMenu);
mockMenu.mockImplementation(()=>{return {update:mockUpdateMenu}});
const expectedResult = {result:true,data:{id:1,name:'updated',description:'test',price:10000,categoryId:1}}
mockUpdateMenu.mockImplementation(async ()=>{return expectedResult});

describe('Test Update Menu',()=>{
    it('should update menu from put menus/menu/:id', async () => {
        const payload = {name:'updated',description:'test',price:10000,categoryId:1}
        const menuId = 2;
        const token = generateDummyAccessToken()
        const res = await request(app).put(`/menus/menu/${menuId}`).send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu.mock.calls[0][0]).toEqual({id:menuId,...payload})
    })
    it('should get response with status 400 given failed update menu', async ()=>{
        const payload = {name:'updated',description:'test',price:10000,categoryId:1}
        const menuId = 2;
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockUpdateMenu.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).put(`/menus/menu/${menuId}`).send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu).toBeCalledTimes(1)
        expect(mockUpdateMenu.mock.calls[0][0]).toEqual({id:menuId,...payload})
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const payload = {name:'updated',description:'test',price:10000,categoryId:1}
        const menuId = 2;
        const res = await request(app).put(`/menus/menu/${menuId}`).send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(401)
        expect(mockMenu).toBeCalledTimes(0)
        expect(mockUpdateMenu).toBeCalledTimes(0)
    })
})