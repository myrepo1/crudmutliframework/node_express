import request from 'supertest';

import app from "../../src/app"

describe('Test Health Route',()=>{
    it('should get response from health route', async () => {
        const res = await request(app).get(`/health`)
        expect(res.statusCode).toEqual(200)
        expect(res.text).toEqual('OK')
    })
})