const mockCategory = jest.fn();
const mockCategoryUpdate = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/category',()=>mockCategory);
mockCategory.mockImplementation(()=>{return {update:mockCategoryUpdate}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test'}]}
mockCategoryUpdate.mockImplementation(async ()=>{return expectedResult});

describe('Test Update Category',()=>{
    it('should update category from put /category/:id', async () => {
        const payload = {name:'test',description:'test'}
        const id = 1;
        const token = generateDummyAccessToken()
        const res = await request(app).put(`/categories/category/${id}`).send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryUpdate).toBeCalledTimes(1)
        expect(mockCategoryUpdate.mock.calls[0][0]).toEqual({id,...payload})
    })
    it('should get response with status 400 given failed create category', async ()=>{
        const payload = {name:'test',description:'test'}
        const id = 1;
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockCategoryUpdate.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).put(`/categories/category/${id}`).send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryUpdate).toBeCalledTimes(1)
        expect(mockCategoryUpdate.mock.calls[0][0]).toEqual({id,...payload})
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const payload = {name:'test',description:'test'}
        const id = 1;
        const res = await request(app).put(`/categories/category/${id}`).send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(401)
    })
})