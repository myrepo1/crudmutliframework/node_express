const mockCategory = jest.fn();
const mockCategoryList = jest.fn();

import request from 'supertest';
import app from "../../../src/app"

jest.mock('../../../src/services/category',()=>mockCategory);
mockCategory.mockImplementation(()=>{return {list:mockCategoryList}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test'}]}
mockCategoryList.mockImplementation(async ()=>{return expectedResult});

describe('Test List Category',()=>{
    it('should get categories from /categories', async () => {
        const res = await request(app).get('/categories')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryList).toBeCalledTimes(1)
    })
    it('should get response with status 400 given failed get categories', async ()=>{
        const expectedFailedList = {result:false,message:'failed'}
        mockCategoryList.mockImplementationOnce(async ()=>{
            return expectedFailedList
        })
        const res = await request(app).get('/categories')
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedList)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryList).toBeCalledTimes(1)
    })
})