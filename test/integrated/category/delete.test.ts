const mockCategory = jest.fn();
const mockCategoryDelete = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/category',()=>mockCategory);
mockCategory.mockImplementation(()=>{return {delete:mockCategoryDelete}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test'}]}
mockCategoryDelete.mockImplementation(async ()=>{return expectedResult});

describe('Test Delete Category',()=>{
    it('should delete category from delete /categories/category/:id', async () => {
        const id = 1;
        const token = generateDummyAccessToken()
        const res = await request(app).delete(`/categories/category/${id}`).set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryDelete).toBeCalledTimes(1)
        expect(mockCategoryDelete.mock.calls[0][0]).toEqual({id})
    })
    it('should get response with status 400 given failed create category', async ()=>{
        const id = 1;
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockCategoryDelete.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).delete(`/categories/category/${id}`).set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryDelete).toBeCalledTimes(1)
        expect(mockCategoryDelete.mock.calls[0][0]).toEqual({id})
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const id = 1;
        const res = await request(app).delete(`/categories/category/${id}`)
        expect(res.statusCode).toEqual(401)
    })
})