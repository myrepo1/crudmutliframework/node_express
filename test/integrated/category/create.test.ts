const mockCategory = jest.fn();
const mockCategoryCreate = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/category',()=>mockCategory);
mockCategory.mockImplementation(()=>{return {create:mockCategoryCreate}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test'}]}
mockCategoryCreate.mockImplementation(async ()=>{return expectedResult});

describe('Test Create Category',()=>{
    it('should create category from post /categories', async () => {
        const payload = {name:'test',description:'test'}
        const token = generateDummyAccessToken()
        const res = await request(app).post('/categories').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(201)
        expect(res.body).toEqual(expectedResult)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryCreate).toBeCalledTimes(1)
        expect(mockCategoryCreate.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 400 given failed create category', async ()=>{
        const payload = {name:'test',description:'test'}
        const expectedFailedCreate = {result:false,message:'failed'}
        const token = generateDummyAccessToken()
        mockCategoryCreate.mockImplementationOnce(async ()=>{
            return expectedFailedCreate
        })
        const res = await request(app).post('/categories').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryCreate).toBeCalledTimes(1)
        expect(mockCategoryCreate.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 401 given no access token provided', async ()=>{
        const payload = {name:'test',description:'test'}
        const res = await request(app).post('/categories').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(401)
    })
    it('should get response with status 401 given wrong data access token', async ()=>{
        const payload = {name:'test',description:'test'}
        const token = generateDummyAccessToken("refreshToken")
        const res = await request(app).post('/categories').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(401)
    })
    it('should get response with status 401 given wrong data access token', async ()=>{
        const payload = {name:'test',description:'test'}
        const token = generateDummyAccessToken("accessToken","wrongSecret")
        const res = await request(app).post('/categories').send(payload).set('Accept', 'application/json').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(401)
    })
})