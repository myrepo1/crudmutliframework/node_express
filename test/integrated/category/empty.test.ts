const mockCategory = jest.fn();
const mockCategoryEmpty = jest.fn();

import request from 'supertest';
import app from "../../../src/app"
import { generateDummyAccessToken } from '../../../src/helpers/security';

jest.mock('../../../src/services/category',()=>mockCategory);
mockCategory.mockImplementation(()=>{return {empty:mockCategoryEmpty}});
const expectedResult = {result:true,data:[{id:1,name:'test',description:'test'}]}
mockCategoryEmpty.mockImplementation(async ()=>{return expectedResult});

describe('Test Empty Category',()=>{
    it('should clear all categories using empty method', async () => {
        const token = generateDummyAccessToken()
        const res = await request(app).delete('/categories').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryEmpty).toBeCalledTimes(1)
    })
    it('should get response with status 400 given failed empty categories', async ()=>{
        const expectedFailedList = {result:false,message:'failed'}
        mockCategoryEmpty.mockImplementationOnce(async ()=>{
            return expectedFailedList
        })
        const token = generateDummyAccessToken()
        const res = await request(app).delete('/categories').set('authorization', `Bearer ${token}`)
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedList)
        expect(mockCategory).toBeCalledTimes(1)
        expect(mockCategoryEmpty).toBeCalledTimes(1)
    })
    it('should get response with status 401 given no token provided', async ()=>{
        const res = await request(app).delete('/categories')
        expect(res.statusCode).toEqual(401)
        expect(mockCategory).toBeCalledTimes(0)
        expect(mockCategoryEmpty).toBeCalledTimes(0)
    })
})