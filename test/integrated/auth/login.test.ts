const mockAuth = jest.fn();
const mockLogin = jest.fn();

import request from 'supertest';
import app from "../../../src/app"

jest.mock('../../../src/services/auth',()=>mockAuth);
mockAuth.mockImplementation(()=>{return {login:mockLogin}});
const expectedResult = {result:true,data:[{accessToken:'1',refreshToken:'2'}]}
mockLogin.mockImplementation(()=>{return expectedResult});

describe('Test Login',()=>{
    it('should return response with status 200 given successful login', async () => {
        const payload = {username:'admin',password:'admin'}
        const res = await request(app).post('/auth/login').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockAuth).toBeCalledTimes(1)
        expect(mockLogin).toBeCalledTimes(1)
        expect(mockLogin.mock.calls[0][0]).toEqual(payload)
    })
    it('should get response with status 400 given failed login', async ()=>{
        const payload = {username:'admin',password:'admin'}
        const expectedFailedCreate = {result:false,message:'failed'}
        mockLogin.mockImplementationOnce(()=>{
            return expectedFailedCreate
        })
        const res = await request(app).post('/auth/login').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockAuth).toBeCalledTimes(1)
        expect(mockLogin).toBeCalledTimes(1)
        expect(mockLogin.mock.calls[0][0]).toEqual(payload)
    })
})