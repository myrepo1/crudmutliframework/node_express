const mockAuth = jest.fn();
const mockRefresh = jest.fn();

import request from 'supertest';
import app from "../../../src/app"

jest.mock('../../../src/services/auth',()=>mockAuth);
mockAuth.mockImplementation(()=>{return {refresh:mockRefresh}});
const expectedResult = {result:true,data:[{accessToken:'1',refreshToken:'2'}]}
mockRefresh.mockImplementation(()=>{return expectedResult});

describe('Test Refresh',()=>{
    it('should return response with status 200 given successful refresh token', async () => {
        const payload = {refreshToken:'1'}
        const res = await request(app).post('/auth/refresh').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expectedResult)
        expect(mockAuth).toBeCalledTimes(1)
        expect(mockRefresh).toBeCalledTimes(1)
        expect(mockRefresh.mock.calls[0][0]).toEqual(payload.refreshToken)
    })
    it('should get response with status 400 given failed refresh token', async ()=>{
        const payload = {refreshToken:'1'}
        const expectedFailedCreate = {result:false,message:'failed'}
        mockRefresh.mockImplementationOnce(()=>{
            return expectedFailedCreate
        })
        const res = await request(app).post('/auth/refresh').send(payload).set('Accept', 'application/json')
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual(expectedFailedCreate)
        expect(mockAuth).toBeCalledTimes(1)
        expect(mockRefresh).toBeCalledTimes(1)
        expect(mockRefresh.mock.calls[0][0]).toEqual(payload.refreshToken)
    })
})