FROM node:alpine
WORKDIR /app
COPY package.json .
RUN npm install
RUN npm install -g dbmate
COPY . .
RUN npm run build
RUN chmod +x /app/entrypoint.sh
EXPOSE 4000
ENTRYPOINT ["/app/entrypoint.sh"]